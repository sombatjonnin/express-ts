import app from './app'
import errorHandler from './middleware/error'
const PORT = 3000
app.use(errorHandler)
if (process.env.NODE_ENV !== 'test') {
  app.listen(app.get('port'), () => {
    console.log('App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'))
    console.log('Press CTRL-C to stop\n')
  })
}
